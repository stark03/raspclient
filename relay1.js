const express = require('express')
const app = express()
const router = express.Router()
const wpi = require('wiring-pi')
wpi.setup('wpi');

router.get('/on/', (req, res, next) => {
    res.status(200).json({
        result: 'success'
    })
    pinMode(15, out)
    analogWrite(15, 0);
    console.log('on')
})
router.get('/off/', (req, res, next) => {
  res.status(200).json({
      result: 'success'
  })
  pinMode(15, out)
  analogWrite(15, 1)
  console.log('off')
})


module.exports = router
